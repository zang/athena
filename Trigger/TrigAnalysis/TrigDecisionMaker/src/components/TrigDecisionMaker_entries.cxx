#include "../TrigDecisionMaker.h"
#include "../TrigDecisionMakerMT.h"
#include "../Lvl1ResultAccessTool.h"

DECLARE_COMPONENT( TrigDec::TrigDecisionMaker )
DECLARE_COMPONENT( TrigDec::TrigDecisionMakerMT )
DECLARE_COMPONENT( HLT::Lvl1ResultAccessTool )
